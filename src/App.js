import React from 'react';
import Header from './components/Header/index';

const App = () => (
    <div className="App">
     <Header />
    </div>
);

export default App;
